type 'a line = 'a list * 'a list

type 'a dnf = 'a line list

type ('elem, 'leaf) bdd =
  | Leaf of 'leaf
  | False
  | Split of
      int * 'elem * ('elem, 'leaf) bdd * ('elem, 'leaf) bdd * ('elem, 'leaf) bdd

module MK
    (E : Custom.T) (Leaf : sig
      include Tset.Tset_base

      val iter : (elem -> unit) -> t -> unit
    end) =
struct
  module X = struct
    type elem = E.t

    type t = (E.t, Leaf.t) bdd

    let rec equal a b =
      a == b
      ||
      (* False, False *)
      match (a, b) with
      | Split (h1, x1, p1, i1, n1), Split (h2, x2, p2, i2, n2) ->
          h1 == h2 && E.equal x1 x2 && equal p1 p2 && equal i1 i2 && equal n1 n2
      | Leaf l1, Leaf l2 -> Leaf.equal l1 l2
      | _ -> false

    let rec compare a b =
      if a == b
      then 0
      else
        match (a, b) with
        | Split (h1, x1, p1, i1, n1), Split (h2, x2, p2, i2, n2) ->
            if h1 < h2
            then -1
            else if h1 > h2
            then 1
            else
              let c = E.compare x1 x2 in
              if c <> 0
              then c
              else
                let c = compare p1 p2 in
                if c <> 0
                then c
                else
                  let c = compare i1 i2 in
                  if c <> 0 then c else compare n1 n2
        | Leaf l1, Leaf l2 -> Leaf.compare l1 l2
        | False, _ -> -1
        | _, False -> 1
        | Leaf _, Split _ -> 1
        | Split _, Leaf _ -> -1

    let hash = function
      | Leaf l                -> Leaf.hash l
      | False                 -> 0
      | Split (h, _, _, _, _) -> h

    let compute_hash x p i n =
      E.hash x + (17 * hash p) + (257 * hash i) + (16637 * hash n)

    let rec check = function
      | False                 -> ()
      | Leaf l                -> Leaf.check l
      | Split (h, x, p, i, n) ->
          assert (h = compute_hash x p i n);
          (match p with
          | Split (_, y, _, _, _) -> assert (E.compare x y < 0)
          | _                     -> ());
          (match i with
          | Split (_, y, _, _, _) -> assert (E.compare x y < 0)
          | _                     -> ());
          (match n with
          | Split (_, y, _, _, _) -> assert (E.compare x y < 0)
          | _                     -> ());
          E.check x;
          check p;
          check i;
          check n

    let empty = False

    let any = Leaf Leaf.any

    let mk_leaf l = if Leaf.(equal l empty) then empty else Leaf l

    let is_true = function Leaf ll -> Leaf.(equal ll any) | _ -> false

    let atom x =
      let h = E.hash x + 17 in
      Split (h, x, any, empty, empty)

    let rec iter_partial fe fleaf = function
      | Split (_, x, p, i, n) ->
          fe x;
          iter_partial fe fleaf p;
          iter_partial fe fleaf i;
          iter_partial fe fleaf n
      | Leaf leaf             -> fleaf leaf
      | _                     -> ()

    let iter_full fe fl = iter_partial fe (Leaf.iter fl)

    let rec dump ppf = function
      | Leaf l                -> Leaf.dump ppf l
      | False                 -> Format.fprintf ppf "-"
      | Split (_, x, p, i, n) ->
          Format.fprintf ppf "%a(@[%a,%a,%a@])" E.dump x dump p dump i dump n

    let rec get accu pos neg = function
      | Leaf leaf             -> ((pos, neg), leaf) :: accu
      | False                 -> accu
      | Split (_, x, p, i, n) ->
          (*OPT: can avoid creating this list cell when pos or neg =False *)
          let accu = get accu (x :: pos) neg p in
          let accu = get accu pos (x :: neg) n in
          let accu = get accu pos neg i in
          accu

    let get (l : t) = get [] [] [] l

    let compute_full ~empty ~any ~cup ~cap ~diff ~atom ~leaf b =
      let rec aux = function
        | Leaf _ as leaf when is_true leaf -> any
        | Leaf l -> leaf l
        | False -> empty
        | Split (_, x, p, i, n) ->
            let atx = atom x in
            let p = cap atx (aux p) and i = aux i and n = diff (aux n) atx in
            cup (cup p i) n
      in
      aux b

    let split0 x pos ign neg =
      Split (compute_hash x pos ign neg, x, pos, ign, neg)

    let rec has_true = function
      | []                 -> false
      | (Leaf _ as e) :: l -> is_true e || has_true l
      | _ :: l             -> has_true l

    let rec has_same a = function
      | []     -> false
      | b :: l -> equal a b || has_same a l

    let rec split x p i n =
      if is_true i
      then any
      else if equal p n
      then p ++ i
      else
        let p = simplify p [ i ] and n = simplify n [ i ] in
        if equal p n then p ++ i else split0 x p i n

    and simplify a l =
      match a with
      | False                 -> False
      | Leaf ll as leaf       ->
          if is_true leaf then if has_true l then False else any else mk_leaf ll
      | Split (_, x, p, i, n) ->
          if has_true l || has_same a l
          then False
          else s_aux2 a x p i n [] [] [] l

    and s_aux2 a x p i n ap ai an = function
      | []     ->
          let p = simplify p ap and n = simplify n an and i = simplify i ai in
          if equal p n then p ++ i else split0 x p i n
      | b :: l -> s_aux3 a x p i n ap ai an l b

    and s_aux3 a x p i n ap ai an l = function
      | False -> s_aux2 a x p i n ap ai an l
      | Leaf ll as leaf ->
          assert (not (is_true leaf));
          mk_leaf ll
      | Split (_, x2, p2, i2, n2) as b ->
          if equal a b
          then False
          else
            let c = E.compare x2 x in
            if c < 0
            then s_aux3 a x p i n ap ai an l i2
            else if c > 0
            then s_aux2 a x p i n (b :: ap) (b :: ai) (b :: an) l
            else s_aux2 a x p i n (p2 :: i2 :: ap) (i2 :: ai) (n2 :: i2 :: an) l

    and ( ++ ) a b =
      if a == b
      then a
      else
        match (a, b) with
        | Leaf _, _ when is_true a -> any
        | _, Leaf _ when is_true b -> any
        | Leaf l1, Leaf l2 -> mk_leaf (Leaf.cup l1 l2)
        | False, a | a, False -> a
        | Split (_, x1, p1, i1, n1), Split (_, x2, p2, i2, n2) ->
            let c = E.compare x1 x2 in
            if c = 0
            then split x1 (p1 ++ p2) (i1 ++ i2) (n1 ++ n2)
            else if c < 0
            then split x1 p1 (i1 ++ b) n1
            else split x2 p2 (i2 ++ a) n2
        | Split (_, x1, p1, i1, n1), Leaf _ -> split x1 p1 (i1 ++ b) n1
        | Leaf _, Split (_, x2, p2, i2, n2) -> split x2 p2 (i2 ++ a) n2

    let rec ( ** ) a b =
      if a == b
      then a
      else
        match (a, b) with
        | _ when is_true a -> b
        | _ when is_true b -> a
        | Leaf l1, Leaf l2 -> mk_leaf (Leaf.cap l1 l2)
        | False, _ | _, False -> False
        | Split (_, x1, p1, i1, n1), Split (_, x2, p2, i2, n2) ->
            let c = E.compare x1 x2 in
            if c = 0
            then
              split x1
                ((p1 ** (p2 ++ i2)) ++ (p2 ** i1))
                (i1 ** i2)
                ((n1 ** (n2 ++ i2)) ++ (n2 ** i1))
            else if c < 0
            then split x1 (p1 ** b) (i1 ** b) (n1 ** b)
            else split x2 (p2 ** a) (i2 ** a) (n2 ** a)
        | Split (_, x1, p1, i1, n1), Leaf _ ->
            split x1 (p1 ** b) (i1 ** b) (n1 ** b)
        | Leaf _, Split (_, x2, p2, i2, n2) ->
            split x2 (p2 ** a) (i2 ** a) (n2 ** a)

    let rec neg = function
      | Leaf _ as l when is_true l -> False
      | Leaf l -> mk_leaf (Leaf.neg l)
      | False -> any
      | Split (_, x, p, i, False) -> split x False (neg (i ++ p)) (neg i)
      | Split (_, x, False, i, n) -> split x (neg i) (neg (i ++ n)) False
      | Split (_, x, p, False, n) -> split x (neg p) (neg (p ++ n)) (neg n)
      | Split (_, x, p, i, n) -> split x (neg (i ++ p)) False (neg (i ++ n))

    let rec ( // ) a b =
      (*    if equal a b then False  *)
      if a == b
      then False
      else
        match (a, b) with
        | False, _ -> False
        | _, _ when is_true b -> False
        | a, False -> a
        | _, b when is_true a -> neg b
        | Leaf l1, Leaf l2 -> mk_leaf (Leaf.diff l1 l2)
        | Split (_, x1, p1, i1, n1), Split (_, x2, p2, i2, n2) ->
            let c = E.compare x1 x2 in
            if c = 0
            then
              if i2 == False && n2 == False
              then split x1 (p1 // p2) (i1 // p2) (n1 ++ i1)
              else
                split x1
                  ((p1 ++ i1) // (p2 ++ i2))
                  False
                  ((n1 ++ i1) // (n2 ++ i2))
            else if c < 0
            then split x1 (p1 // b) (i1 // b) (n1 // b)
            else split x2 (a // (i2 ++ p2)) False (a // (i2 ++ n2))
        | Split (_, x1, p1, i1, n1), Leaf _ ->
            split x1 (p1 // b) (i1 // b) (n1 // b)
        | Leaf _, Split (_, x2, p2, i2, n2) ->
            split x2 (a // (i2 ++ p2)) False (a // (i2 ++ n2))

    let cup = ( ++ )

    let cap = ( ** )

    let diff = ( // )
  end

  include X
  module Infix = Tset.MakeInfix (X)
end

module Unit : sig
  include Tset.Tset_base with type t = unit

  val iter : (elem -> unit) -> t -> unit
end = struct
  type t = unit

  type elem = unit

  let check () = ()

  let equal () () = true

  let compare () () = 0

  let hash () = 1

  let dump ppf () = Format.fprintf ppf "+"

  let empty = () (* invalid *)

  let any = ()

  let atom () = ()

  let cup () () = ()

  let cap () () = ()

  let neg () = assert false

  let diff () () = assert false

  let iter _ () = ()
end

module type S = sig
  include Custom.T

  type atom
  (** The type of atoms in the Boolean combinations *)

  type mono
  (** The type of Boolean combinations of atoms. *)

  type line
  (** An explicit representation of conjunctions of atoms. *)

  type dnf
  (** An explicit representation fo the DNF. *)

  val atom : atom -> t

  val mono : mono -> t

  val mono_dnf : mono -> dnf
  val any : t

  val empty : t

  val cup : t -> t -> t

  val cap : t -> t -> t

  val diff : t -> t -> t

  val neg : t -> t

  val get : t -> dnf

  val get_mono : t -> mono

  val iter : (atom -> unit) -> t -> unit

  val compute :
    empty:'b ->
    any:'b ->
    cup:('b -> 'b -> 'b) ->
    cap:('b -> 'b -> 'b) ->
    diff:('b -> 'b -> 'b) ->
    atom:(atom -> 'b) ->
    t ->
    'b

  val var : Var.t -> t
  (** {2 Polymorphic interface. }*)

  val get_partial : t -> ((Var.t list * Var.t list) * mono) list

  val get_full : t -> ((Var.t list * Var.t list) * line) list

  val iter_partial : (Var.t -> unit) -> (mono -> unit) -> t -> unit

  val iter_full : (Var.t -> unit) -> (atom -> unit) -> t -> unit

  val compute_partial :
    empty:'b ->
    any:'b ->
    cup:('b -> 'b -> 'b) ->
    cap:('b -> 'b -> 'b) ->
    diff:('b -> 'b -> 'b) ->
    mono:(mono -> 'b) ->
    var:(Var.t -> 'b) ->
    t ->
    'b

  val compute_full :
    empty:'b ->
    any:'b ->
    cup:('b -> 'b -> 'b) ->
    cap:('b -> 'b -> 'b) ->
    diff:('b -> 'b -> 'b) ->
    atom:(atom -> 'b) ->
    var:(Var.t -> 'b) ->
    t ->
    'b

  module Infix : sig
    val ( ++ ) : t -> t -> t

    val ( ** ) : t -> t -> t

    val ( // ) : t -> t -> t

    val ( ~~ ) : t -> t
  end
end

module Make (E : Custom.T) :
  S
    with type atom = E.t
     and type line = E.t list * E.t list
     and type dnf = (E.t list * E.t list) list = struct
  module Atom = MK (E) (Unit)

  include
    MK
      (Var)
      (struct
        include Atom

        let iter f b = iter_full f ignore b
      end)

  type atom = E.t

  type line = E.t list * E.t list

  type dnf = (E.t list * E.t list) list

  type mono = Atom.t

  let var x = atom x

  let atom x = Leaf (Atom.atom x)

  let mono x = Leaf x

  let get_aux combine acc b =
    List.fold_left
      (fun acc ((p, n), dnf) ->
        let dnf = Atom.get dnf in
        List.fold_left (fun acc ((p2, n2), ()) -> combine p n p2 n2 acc) acc dnf)
      acc (get b)

  let get_full = get_aux (fun p n p2 n2 acc -> ((p, n), (p2, n2)) :: acc) []

  let mono_dnf mono = List.map fst @@ Atom.get mono

  let get_partial b : ((Var.t list * Var.t list) * mono) list = get b

  let get b = List.rev @@ get_aux (fun _ _ p2 n2 acc -> (p2, n2) :: acc) [] b

  let get_mono b : mono =
    List.fold_left (fun acc (_, dnf) -> Atom.cup acc dnf) empty (get_partial b)

  (* Temporary List.rev to order elements as before introduction of
     polymorphic variables.
  *)
  let iter = iter_full (fun _ -> ())

  let compute_partial ~empty ~any ~cup ~cap ~diff ~mono ~(var : Var.t -> 'a) b =
    compute_full ~empty ~any ~cup ~cap ~diff ~atom:var ~leaf:mono b

  let compute_full ~empty ~any ~cup ~cap ~diff ~atom ~(var : Var.t -> 'a) b =
    compute_full ~empty ~any ~cup ~cap ~diff ~atom:var
      ~leaf:
        (Atom.compute_full ~empty ~any ~cup ~cap ~diff ~atom ~leaf:(fun () ->
             any))
      b

  let compute ~empty ~any ~cup ~cap ~diff ~atom b =
    compute_full ~empty ~any ~cup ~cap ~diff ~atom ~var:(fun _ -> any) b
end

module MKBasic (T : Tset.S) :
  S with type atom = T.elem and type mono = T.t and type dnf = T.t = struct
  include
    MK
      (Var)
      (struct
        include T

        let iter _ _ = assert false
      end)

  type line

  type atom = T.elem

  type mono = T.t

  type dnf = T.t

  let var v = atom v

  let mono x = Leaf x

  let atom x = Leaf (T.atom x)

  let get_partial t = get t

  let mono_dnf x = x

  let get_mono t =
    List.fold_left (fun acc ((_, _), dnf) -> T.cup acc dnf) T.empty (get t)

  let get _ = assert false

  let get_full _ = assert false

  let compute_partial ~empty ~any ~cup ~cap ~diff ~mono ~var b =
    compute_full ~empty ~any ~cup ~cap ~diff ~atom:var ~leaf:mono b

  let[@ocaml.warning "-27"] compute_full ~empty ~any ~cup ~cap ~diff ~atom ~var
      b =
    assert false

  let[@ocaml.warning "-27"] compute ~empty ~any ~cup ~cap ~diff ~atom b =
    assert false

  let iter _ _ = assert false

  let iter_full _ _ _ = assert false
end

module VarIntervals = MKBasic (Intervals)
module VarCharSet = MKBasic (CharSet)
module VarAtomSet = MKBasic (AtomSet)
module VarAbstractSet = MKBasic (AbstractSet)
