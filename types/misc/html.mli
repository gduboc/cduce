type t

val create: bool -> t
val ppf: t -> Format.formatter
val get: t -> string
val mark: t -> string -> unit
val markup: t -> string -> (Format.formatter -> unit) -> unit
val is_html: t -> bool

