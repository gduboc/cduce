(**
  The minimal interface for a set of values to be part of a type.
  All kinds (for instance the ℂDuce type [Int] {!module:Intervals})
  support at least these operations.
*)

(** This module represent the basic signature for sets. It should not be used
  directly, see {!S}.
*)
module type Tset_base = sig

  (** The type of the values in the set *)
  type elem

  (** The type of the set, with mandatory custom operations. *)
  include Custom.T

  (** The empty set *)
  val empty : t

  (** The full set, containing all possible values for this kind.*)
  val any : t

  (** [atom e] creates a singleton set containing element [e]. *)
  val atom : elem -> t

  (** {2 Set operations :}*)

  (** [cup t1 t2] returns the unions of [t1] and [t2]. *)
  val cup : t -> t -> t

  (** [cap t1 t2] returns the intersection of [t1] and [t2]. *)
  val cap : t -> t -> t

  (** [diff t1 t2] returns the set of elements of [t1] not in [t2]. *)
  val diff : t -> t -> t

  (** [neg t] returns the set [diff any t]. *)
  val neg : t -> t
end

(** The full signature of a set of value. It contains the basic operations as 
well as some infix operators. *)
module type S = sig
  include Tset_base

  module Infix : sig

    (** [t1 ++ t2] is an alias for [cup t1 t2]*)
    val ( ++ ) : t -> t -> t

    (** [t1 ** t2] is an alias for [cap t1 t2]*)
    val ( ** ) : t -> t -> t

    (** [t1 // t2] is an alias for [diff t1 t2]*)
    val ( // ) : t -> t -> t

    (** [~~ t] is an alias for [neg t]*)
    val ( ~~ ) : t -> t
  end
end

(** This is a convenience module to automatically generate infix operators from
  a module implementing the basic operations.*)
module MakeInfix (X : Tset_base) =
struct
  let ( ++ ) = X.cup
  let ( ** ) = X.cap
  let ( // ) = X.diff
  let ( ~~ ) = X.neg
end
