# Testing

## Layout
Testing is split in several categories. Each category contains a `good/` and
optionally a `bad` directory for expected to succeed and expected to fail tests.
It also contains a `src` directory with the code used to generate the
`{good,bad}/dune.auto` file containing all the tests (since at the moment,
`dune` does not support generic rules with wildcard patterns). Lastly a `common`
directory may be present to share code and input files between `good` and `bad`.

- `full`: 
    - `good`: compiles a CDuce program, runs it and diffs its output against an expected output. The
    compilation must succeed, the execution must succeed and the output must be identical to the
    expeced one.
    - `bad`: compiles a CDuce program, runs it and diffs its output against an expected output. The
    compilation must succeed, but the execution must fail with an error message which is checked
    agains the expected file.

- `parsing`:
    - `good`:
    - `bad`:

- `typing`:
    - `good`:
    - `bad`:

- `ocaml_ext`:
    - `good`: Like `full/good` but uses OCaml primitives embeded in the runtime
    - `bad`: Like `full/bad` but uses OCaml primitives embeded in the runtime

## Running tests
Tests are simply run by invoking 
```
dune runtest
```


## Changing the result of a test
Expected results of tests are in `.exp` files. When the result of an *existing*
test changes (and thus the diff with the `.exp` file fails), one needs to
promote the new result as the reference one, using
```
dune promote
```
right after the failing ```dune runtest```.

## Adding test
To add a new test, one needs to add the file in the relevent directory (together
with its data or code dependencies) and run 
```
dune build @depend --auto-promote
```
This will list all the tests in each test directory and will output for each one a set of testing
rules in the correct `dune` file.

## Adding a test category
A category consists of several elements:
 * a directory structure (/e.g./ `good/`, `bad/`, …)
 * a `dune` file in each directory containing tests to be run
 * a `dune.auto` file, containing the list of all testing rules for the directory
 * a `src/gen_dune.ml` file which is a program used by the `dune` file to generate the `dune.auto`
   file. This program can use the private `lib_test` library.