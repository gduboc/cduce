val sdl_version : string

val sdl_init : unit
val sdl_quit : unit -> unit

val sdl_num_drives : int

val sdl_cd_open : int -> Sdlcdrom.cdrom_drive
val sdl_cd_eject : Sdlcdrom.cdrom_drive -> unit
