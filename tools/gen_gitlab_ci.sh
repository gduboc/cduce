#!/bin/sh

echo "
workflow:
  rules:
    - if: \$CI_COMMIT_REF_NAME =~ /^monomorphic$|^master$|^polymorphic$/
      when: always
    - when: never

stages:
    - compile
    - test
    - package

.compile_template: &compile_template
  stage: compile
  script:
    - dune build @binary

.test_template: &test_template
  stage: test
  script:
    - dune build @runtest

.package_template: &package_template
  stage: package
  script:
    - opam pin -y cduce-types .
    - opam pin -y cduce .
    - opam pin -y cduce-tools .
"

for v in 4.07.1 4.08.1 4.09.1 4.10.2 4.11.2 4.12.1 4.13.1 4.12.0-domains
do
    p=""
    for s in compile test package
    do
    echo "${s}_${v}:"
    echo "  image: ocaml-cduce:${v}"
    echo -n "$p"
    echo "  <<: *${s}_template"
    echo
    p="  needs: [ \"${s}_${v}\" ]
"
    done
    echo
done
