#!/bin/sh

#This script is used to initialize a freshly created opam switch
#with CDuce PINS
#Change these variables to suit your needs
GIT_REPO="git+https://gitlab.math.univ-paris-diderot.fr/cduce/cduce"
BRANCH="polymorphic"
PACKAGES="cduce-types cduce"

DO_INSTALL=""
DO_PIN=""
DO_PRINT=""
for i in "$@"
do
    if test "$i" = "--install"; then
        DO_INSTALL=1
    elif test "$i" = "--pin"; then
        DO_PIN=1
    elif test "$i" = "--print-deps"; then
        DO_PRINT=1
    elif test "$i" = "--help"; then
        DO_INSTALL=""
        DO_PIN=""
        DO_PRINT=""
        break
    fi
done

if test -z "$DO_INSTALL" -a -z "$DO_PIN" -a -z "$DO_PRINT"; then
    echo "Usage: $0 [options]"
    echo
    echo "options:"
    echo "  --install    install the developpment dependencies via opam"
    echo "  --pin        execute opam pin add on the git devel branch"
    echo "  --print-deps print the development dependencies"
    echo "  --help       display this message"
    exit 0
fi

##

OPAM=`which opam`
SCRIPT_PATH=`dirname $0`
BASE_PATH="$(cd ${SCRIPT_PATH}/..; pwd)"
if test -z "$OPAM" ;
then
    echo "Error: cannot find opam in PATH, is it installed ?"
    exit 1
fi
DEPS="${PACKAGES}"
for p in $PACKAGES
do
    OFILE="${BASE_PATH}/${p}.opam"
    if test -f "$OFILE";
    then
        DEPS="${DEPS} $(opam show --file=${OFILE} -f depopts:,depends: |\
                        sed -e 's/[{][^}]*[}]\|[()|]\|^depopts:\|^depends://g' |\
                        sed -e 's/[^"]*["]\([^"]*\)["]/\1 /g' |\
                        xargs echo)"
    else
        echo "Error: cannot find opam file ${p}.opam in $(cd ${SCRIPT_PATH}/..; pwd)"
        exit 2
    fi
done
DEPS="$(for i in ${DEPS}; do echo "${i}"; done | sort -u | xargs echo)"
RDEPS=""
for d in ${DEPS};
do
    q="$d"
    for p in ${PACKAGES};
    do
        if test "$d" = "$p"; then
        q=""
        fi
    done
    RDEPS="${RDEPS} $q"
done
if test "$DO_PRINT"; then
    echo "$RDEPS"
fi
if test "$DO_PIN"; then
    "$OPAM" pin -y --no-action add cduce-types "${GIT_REPO}#${BRANCH}"
    "$OPAM" pin -y --no-action add cduce "${GIT_REPO}#${BRANCH}"
fi
if test "$DO_INSTALL"; then
"$OPAM" install -y $RDEPS
fi
